```{include} ../README.md
```

```{toctree}
:maxdepth: 1
:hidden:

installation.md
getting_started.ipynb
tutorial.md
changelog.md
contributing.md
conduct.md
autoapi/index
```