:py:mod:`psychopy_bids.bids`
============================

.. py:module:: psychopy_bids.bids

.. autoapi-nested-parse::

   Package to support the creation of valid bids-datasets.



Submodules
----------
.. toctree::
   :titlesonly:
   :maxdepth: 1

   bidshandler/index.rst
   bidstaskevent/index.rst


Package Contents
----------------

Classes
~~~~~~~

.. autoapisummary::

   psychopy_bids.bids.BIDSTaskEvent
   psychopy_bids.bids.BIDSHandler




.. py:class:: BIDSTaskEvent(onset, duration, *arg, trial_type=None, sample=None, response_time=None, value=None, hed=None, stim_file=None, identifier=None, database=None, **kw)

   Bases: :py:obj:`dict`

   A BIDSTaskEvent describes timing and other properties of events recorded during a run.
   Events are, for example, stimuli presented to the participant or participant responses.

   .. attribute:: onset

      Onset (in seconds) of the event, measured from the beginning of the acquisition of the
      first data point stored in the corresponding task data file.

      :type: int, float

   .. attribute:: duration

      Duration of the event (measured from onset) in seconds.

      :type: int, float, 'n/a'

   .. attribute:: trial_type

      Primary categorisation of each trial to identify them as instances of the experimental
      conditions.

      :type: str

   .. attribute:: sample

      Onset of the event according to the sampling scheme of the recorded modality.

      :type: int

   .. attribute:: response_time

      Response time measured in seconds.

      :type: int, float, 'n/a'

   .. attribute:: value

      Marker value associated with the event.

      :type: Any

   .. attribute:: hed

      Hierarchical Event Descriptor (HED) Tag.

      :type: str

   .. attribute:: stim_file

      Represents the location of the stimulus file (such as an image, video, or audio file)
      presented at the given onset time.

      :type: str

   .. attribute:: identifier

      Represents the database identifier of the stimulus file presented at the given onset
      time.

      :type: str

   .. attribute:: database

      Represents the database of the stimulus file presented at the given onset time.

      :type: str

   .. py:property:: onset

      Onset (in seconds) of the event, measured from the beginning of the acquisition of the
      first data point stored in the corresponding task data file.

   .. py:property:: duration

      Duration of the event (measured from onset) in seconds.

   .. py:property:: trial_type

      Primary categorisation of each trial to identify them as instances of the experimental
      conditions.

   .. py:property:: sample

      Onset of the event according to the sampling scheme of the recorded modality.

   .. py:property:: response_time

      Response time measured in seconds.

   .. py:property:: value

      Marker value associated with the event.

   .. py:property:: hed

      Hierarchical Event Descriptor (HED) Tag.

   .. py:property:: stim_file

      Represents the location of the stimulus file (such as an image, video, or audio file)
      presented at the given onset time.

   .. py:property:: identifier

      Represents the database identifier of the stimulus file presented at the given onset time.

   .. py:property:: database

      Represents the database of the stimulus file presented at the given onset time.

   .. py:method:: __repr__()

      Return repr(self).


   .. py:method:: from_dict(dictionary)

      Converts a dictionary into a BIDSTaskEvent object.

      :param dictionary: a dictionary representing a task event.
      :type dictionary: dict

      :returns: BIDSTaskEvent object representing all column names of a task event
      :rtype: BIDSTaskEvent

      .. rubric:: Examples

      >>> event = BIDSTaskEvent(0, 0)
      >>> event.from_dict({'onset': 1, 'duration': 1})
      BIDSTaskEvent(1, 1)


   .. py:method:: to_dict()

      Converts a BIDSTaskEvent object to a dictionary.

      :returns: Dictionary representing all column names of a task event
      :rtype: dict

      .. rubric:: Examples

      >>> BIDSTaskEvent(1, 1).to_dict()
      {'onset': 1, 'duration': 1, 'trial_type': None, 'sample': None, 'response_time': None,
      'value': None, 'hed': None, 'stim_file': None, 'identifier': None, 'database': None}



.. py:exception:: BIDSError

   Bases: :py:obj:`Exception`

   Base class for all BIDS Exceptions


.. py:class:: BIDSHandler(dataset, subject, task, session=None, data_type='beh', acq=None)

   A class to handle the BIDSTaskEvents saved in the PsychoPy ExperimentHandler.

   .. attribute:: `dataset`

      A set of neuroimaging and behavioral data acquired for a purpose of a particular study.

      :type: str

   .. attribute:: `subject`

      A person or animal participating in the study.

      :type: str

   .. attribute:: `task`

      A set of structured activities performed by the participant.

      :type: int

   .. attribute:: `session`

      A logical grouping of neuroimaging and behavioral data consistent across subjects.

      :type: int

   .. attribute:: `data_type`

      A functional group of different types of data.

      :type: str

   .. attribute:: `acq`

      Custom label to distinguish different conditions present during multiple runs of the
      same task

      :type: str

   .. py:property:: dataset

      A set of neuroimaging and behavioral data acquired for a purpose of a particular study.

   .. py:property:: subject

      A participant identifier of the form sub-<label>, matching a participant entity found in
      the dataset.

   .. py:property:: task

      A set of structured activities performed by the participant.

   .. py:property:: session

      A logical grouping of neuroimaging and behavioral data consistent across subjects.

   .. py:property:: data_type

      A functional group of different types of data.

   .. py:property:: acq

      A label to distinguish a different set of parameters used for acquiring the same modality.

   .. py:method:: addJSONSidecar(event_file, existing_file=None, version=None)

      Add an accompanying JSON sidecar file for supporting documentation of task event files.

      :param `event_file`: path of the accompanying task event file.
      :type `event_file`: str
      :param `existing_file`: path to an existing sidecar JSON file.
      :type `existing_file`: str
      :param `version`: software version used in the experiment.
      :type `version`: str

      .. rubric:: Examples

      >>> handler.addJSONSidecar("sub-01_ses-1_task-A_run-1_events.tsv", "temp.json", "2023.1.0")


   .. py:method:: addStimuliFolder(event_file, path='stimuli')

      Add a `/stimuli` directory (under the root directory of the dataset; with OPTIONAL
      subdirectories). The values under the stim_file column correspond to a path relative to
      /stimuli. For example images/cat03.jpg will be translated to /stimuli/images/cat03.jpg.

      :param `event_file`: path of the accompanying task event file.
      :type `event_file`: str
      :param `path`: path to the individual experiment stimuli.
      :type `path`: str

      .. rubric:: Examples

      >>> handler.addStimuliFolder("sub-01_ses-1_task-simple_run-1_events.tsv")


   .. py:method:: addTaskEvents(events, participant_info)

      Add all task events of one task to the subject folder.

      :param `task`: a set of structured activities performed by the participant.
      :type `task`: ExperimentHandler
      :param `participant_info`: a dictionary describing properties of each participant (such as age, sex, etc.)
      :type `participant_info`: dict

      :returns: **`file_name`** -- path of the created tsv event file.
      :rtype: str

      .. rubric:: Examples

      >>> event_list = [{'trigger': bids.BIDSTaskEvent(onset=1.0, duration=0}]
      >>> participant_info = {'participant_id': handler.subject, 'age': 18}
      >>> handler.addTaskEvents(event_list, participant_info)


   .. py:method:: createDataset()

      Creates the rudimentary body of a new dataset.

      .. rubric:: Examples

      >>> handler.createDataset()


   .. py:method:: parseLog(file, level='BIDS', regex=None)

      Updates a task with all task events (from a specific log file).

      :param `file`: file path of the log file.
      :type `file`: str
      :param `level`: level name of the bids task events.
      :type `level`: str
      :param `regex`: regular expression to parse the message string
      :type `regex`: str

      :returns: **`events`** -- a list of events like presented stimuli or participant responses
      :rtype: list

      .. rubric:: Examples

      >>> events = handler.parseLog("simple.log", "BIDS")



