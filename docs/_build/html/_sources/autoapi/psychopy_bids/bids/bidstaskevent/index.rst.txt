:py:mod:`psychopy_bids.bids.bidstaskevent`
==========================================

.. py:module:: psychopy_bids.bids.bidstaskevent

.. autoapi-nested-parse::

   This module provides a class to save your TaskEvents in the BIDS data structure



Module Contents
---------------

Classes
~~~~~~~

.. autoapisummary::

   psychopy_bids.bids.bidstaskevent.BIDSTaskEvent




.. py:class:: BIDSTaskEvent(onset, duration, *arg, trial_type=None, sample=None, response_time=None, value=None, hed=None, stim_file=None, identifier=None, database=None, **kw)

   Bases: :py:obj:`dict`

   A BIDSTaskEvent describes timing and other properties of events recorded during a run.
   Events are, for example, stimuli presented to the participant or participant responses.

   .. attribute:: onset

      Onset (in seconds) of the event, measured from the beginning of the acquisition of the
      first data point stored in the corresponding task data file.

      :type: int, float

   .. attribute:: duration

      Duration of the event (measured from onset) in seconds.

      :type: int, float, 'n/a'

   .. attribute:: trial_type

      Primary categorisation of each trial to identify them as instances of the experimental
      conditions.

      :type: str

   .. attribute:: sample

      Onset of the event according to the sampling scheme of the recorded modality.

      :type: int

   .. attribute:: response_time

      Response time measured in seconds.

      :type: int, float, 'n/a'

   .. attribute:: value

      Marker value associated with the event.

      :type: Any

   .. attribute:: hed

      Hierarchical Event Descriptor (HED) Tag.

      :type: str

   .. attribute:: stim_file

      Represents the location of the stimulus file (such as an image, video, or audio file)
      presented at the given onset time.

      :type: str

   .. attribute:: identifier

      Represents the database identifier of the stimulus file presented at the given onset
      time.

      :type: str

   .. attribute:: database

      Represents the database of the stimulus file presented at the given onset time.

      :type: str

   .. py:property:: onset

      Onset (in seconds) of the event, measured from the beginning of the acquisition of the
      first data point stored in the corresponding task data file.

   .. py:property:: duration

      Duration of the event (measured from onset) in seconds.

   .. py:property:: trial_type

      Primary categorisation of each trial to identify them as instances of the experimental
      conditions.

   .. py:property:: sample

      Onset of the event according to the sampling scheme of the recorded modality.

   .. py:property:: response_time

      Response time measured in seconds.

   .. py:property:: value

      Marker value associated with the event.

   .. py:property:: hed

      Hierarchical Event Descriptor (HED) Tag.

   .. py:property:: stim_file

      Represents the location of the stimulus file (such as an image, video, or audio file)
      presented at the given onset time.

   .. py:property:: identifier

      Represents the database identifier of the stimulus file presented at the given onset time.

   .. py:property:: database

      Represents the database of the stimulus file presented at the given onset time.

   .. py:method:: __repr__()

      Return repr(self).


   .. py:method:: from_dict(dictionary)

      Converts a dictionary into a BIDSTaskEvent object.

      :param dictionary: a dictionary representing a task event.
      :type dictionary: dict

      :returns: BIDSTaskEvent object representing all column names of a task event
      :rtype: BIDSTaskEvent

      .. rubric:: Examples

      >>> event = BIDSTaskEvent(0, 0)
      >>> event.from_dict({'onset': 1, 'duration': 1})
      BIDSTaskEvent(1, 1)


   .. py:method:: to_dict()

      Converts a BIDSTaskEvent object to a dictionary.

      :returns: Dictionary representing all column names of a task event
      :rtype: dict

      .. rubric:: Examples

      >>> BIDSTaskEvent(1, 1).to_dict()
      {'onset': 1, 'duration': 1, 'trial_type': None, 'sample': None, 'response_time': None,
      'value': None, 'hed': None, 'stim_file': None, 'identifier': None, 'database': None}



.. py:exception:: BIDSError

   Bases: :py:obj:`Exception`

   Base class for all BIDS Exceptions


.. py:exception:: OnsetError(onset, msg="Property 'onset' MUST be a number")

   Bases: :py:obj:`BIDSError`

   Raised if onset is not correct

   .. attribute:: onset

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: DurationError(duration, msg="Property 'duration' MUST be either zero or positive (or n/a if unavailable)")

   Bases: :py:obj:`BIDSError`

   Raised if duration is not correct

   .. attribute:: duration

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: TrialTypeError(trial_type, msg="Property 'trial_type' MUST be a string")

   Bases: :py:obj:`BIDSError`

   Raised if trial_type is not correct

   .. attribute:: trial_type

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: SampleError(sample, msg="Property 'sample' MUST be an integer")

   Bases: :py:obj:`BIDSError`

   Raised if sample is not correct

   .. attribute:: sample

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: ResponseTimeError(response_time, msg="Property 'response_time' MUST be a number (or n/a if unavailable)")

   Bases: :py:obj:`BIDSError`

   Raised if response_time is not correct

   .. attribute:: response_time

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: HedError(hed, msg="Property 'hed' MUST be a string")

   Bases: :py:obj:`BIDSError`

   Raised if hed is not correct

   .. attribute:: hed

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: StimFileError(stim_file, msg="Property 'stim_file' MUST be a string")

   Bases: :py:obj:`BIDSError`

   Raised if stim_file is not correct

   .. attribute:: stim_file

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: IdentifierError(identifier, msg="Property 'identifier' MUST be a string")

   Bases: :py:obj:`BIDSError`

   Raised if identifier is not correct

   .. attribute:: identifier

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



.. py:exception:: DatabaseError(database, msg="Property 'database' MUST be a string")

   Bases: :py:obj:`BIDSError`

   Raised if database is not correct

   .. attribute:: database

      Input which caused the error

      :type: object

   .. attribute:: msg

      Explanation of the error

      :type: str

   .. py:method:: __str__()

      Return str(self).



