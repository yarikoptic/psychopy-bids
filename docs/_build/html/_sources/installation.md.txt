# Installation

## psychopy-bids Installation for Windows

A simple guide to install **PsychoPy** as a standalone version together with **psychopy_bids** and add the new builder components. 

#### **Step 1:** Install PsychoPy Standalone Package

First install the standalone version of Pschopy, you can download and install it from the official website or click [here](https://www.psychopy.org/download.html).

#### **Step 2:** Install psychopy-bids

First open your terminal. To do this 
1. Select the Start button. 
2. Type "cmd". 
3. Select Command Prompt from the list.

To avoid possible problems, it is best to start the Command Promt as aministrator.

![Command Prompt](img/inst-fig01.png)

To install **psychopy-bids** use ``"path\python.exe" -m pip install psychopy_bids``. If you have installed PsychoPy to the standard installation folder, your path is most likely going to be ``C:\Program Files\PsychoPy``. In this case you would run the command ``"C:\Program Files\PsychoPy\python.exe" -m pip install psychopy_bids``

#### **Step 3:** Add the Builder Elements to the Psychopy Builder

To add the Builder component and routine from **psychopy-bids** to PsychoPy you have to run the function `addBuilderElements()` once in a python interpreter.
Do this by starting your PsychoPy python session by running ``"path\python.exe"``. If you have installed PsychoPy to the standard installation folder, your path is most likely going to be ``C:\Program Files\PsychoPy``. In this case you would run the command ``"C:\Program Files\PsychoPy\python.exe"``

You can now execute the commands:
```python
import psychopy_bids

psychopy_bids.addBuilderElements()
```

#### **Step 4:** Use the Components

As you can see in the next figure the **BIDS Events component** and the **BIDS Export routine** are now available and ready to use.

![Original PsychoPy Builder](img/inst-fig02.png)