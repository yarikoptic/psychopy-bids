## Psychopy Builder Tutorials

In the following two tutorials we want to use PsychoPy's built-in demos **stroop** and **mentalRotation** to extend them with our BIDS components. If you do not have any demos available yet, you must first unpack them to the desired location.

![Unpack Demos](img/build-fig01.png)

### stroop

#### **Step 1:** open stroop demo

To be able to modify the demo, it must be opened first.

![Original stroop demo](img/stroop-fig01.png)

If this is successful you should see the following experiment flow in PsychoPy Builder:

![Original stroop demo](img/stroop-fig02.png)

#### **Step 2:** create Bids Event

Select the trial routine and then click on the **BIDS Event** component in the menu on the right. Fill in the individual properties of the component with the following values:

 - **Name**: *bidsEvent*\
   The name you want to use to address the component.
 - **Onset**: *resp.tStartRefreh*\
   Onset (in seconds) of the event, measured from the beginning of the experiment.
 - **Duration**: *resp.rt*\
   Duration (in seconds) of the event.
 - **Trial type**: *$f"{text}_{letterColor}"*\
   Categorisation of each trial. In this case the text and color of the stimulus.
 - **Response time**: *resp.rt*\
   The reaction time of the subject in the trial.

![Original stroop demo](img/stroop-fig03.png)

In the **More** menu item, you must also fill in the following property:
 - **Value**: *$resp.keys*\
   The pressed key in the trial.

![Original stroop demo](img/stroop-fig04.png)

#### **Step 3:** create Bids Export

In order to save your previously defined event in a suitable structure, a **BIDS Export routine** must be defined. You can also find this in the menu on the right. For this example, leave all default settings and append your routine at the end of the experiment flow.

![Original stroop demo](img/stroop-fig05.png)

#### **Step 4:** Run experiment and check output

Now you can start your experiment. After a first run, your folder structure and tsv-file should look like this:

![Original stroop demo](img/stroop-fig06.png)

### mentalRotation

#### **Step 1:** open mentalRotation demo

To be able to change the demo, it must first be opened. You should now be able to see and open the mentalRotation demo.

![Original mentalRotation demo](img/ment-fig02.png)

If this is successful you should see the following experiment flow in PsychoPy Builder:

![Original mentalRotation demo flow](img/ment-fig03.png)

TODO